Walker = Object:extend()

function Walker:new(drawMode, pos_x, pos_y, radius)
	self.id = 1
	self.steps = 0

	self.drawMode = drawMode
	self.radius = radius
	self.position = { x = pos_x, y = pos_y }
	self.color = { 0, 0, 0, 1 }
end

function Walker:update(width, height)
	local x_step = love.math.random(-1, 1)
	local y_step = love.math.random(-1, 1)

	self.position.x = self.position.x + x_step
	self.position.y = self.position.y + y_step

	self.steps = self.steps + 1

	if self.position.x < 0 or self.position.x > width or self.position.y < 0 or self.position.y > height then
		self.id = self.id + 1
		self.steps = 0
		self.position.x = width / 2
		self.position.y = height / 2
		self.color = { love.math.random(), love.math.random(), love.math.random(), 1 }
	end
end

function Walker:draw()
	love.graphics.setColor(self.color)
	love.graphics.circle(self.drawMode, self.position.x, self.position.y, self.radius)
end

-- Getters
function Walker:get_id()
	return self.id
end

function Walker:get_steps()
	return self.steps
end
