function love.load()
	Object = require("lib/classic")
	require("src/walker")

	width, height = love.graphics.getDimensions()
	walker = Walker("fill", width / 2, height / 2, 3)

	-- Window settings
	love.window.setTitle("Random Walker with Love")
	love.window.setMode(800, 600)

	-- Canvas settings
	canvas = love.graphics.newCanvas(800, 600)
	love.graphics.setCanvas(canvas)
	love.graphics.clear()
	love.graphics.setBackgroundColor(1, 1, 1)
	love.graphics.setBlendMode("alpha")

	love.graphics.setCanvas()

	-- show info settings
	show_info_status = true
end

function love.keypressed(key)
	if key == "f5" then
		show_info_status = not show_info_status
	end
end

function love.update()
	walker:update(width, height)
end

function love.draw()
	love.graphics.setCanvas(canvas)
	walker:draw()

	love.graphics.setCanvas()
	love.graphics.draw(canvas)

	if show_info_status then
		show_info()
	end
end

function show_info()
	local x = 20
	local y = 20

	love.graphics.setColor(0, 0, 0)
	love.graphics.print("Info", x, y)
	love.graphics.print("\tId: " .. walker:get_id(), x, y * 2)
	love.graphics.print("\tSteps: " .. walker:get_steps(), x, y * 3)
end

