# Random Walker
The random walker algorithm is an algorithm for image segmentation. This repo contains my implementation of the algorithm.

## Implementations
- lua
    - [love 2D](./love-2d)
- C++
    - [SFML](./sfml)

