#include "../header/window.h"

Window::Window(const std::string& l_title, const sf::Vector2u l_size, const int& l_framerate)
{
    this->m_title = l_title;
    this->m_windowSize = l_size;
    this->m_framerate = l_framerate;
    
    this->m_isDone = false;

    this->Setup();
}

void Window::Setup()
{
    this->m_window.create({this->m_windowSize.x, this->m_windowSize.y}, this->m_title);
    this->m_window.setFramerateLimit(this->m_framerate);
}

void Window::Update() 
{
    sf::Event event;

    while (this->m_window.pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
           this->m_isDone = true;
        } 
        else if (event.type == sf::Event::Resized)
        {
            this->BeginDraw();
        }
    }
}

void Window::BeginDraw()
{
    this->m_window.clear(sf::Color::White);
}

void Window::Draw(sf::Drawable &l_drawnable)
{
    this->m_window.draw(l_drawnable);
}

void Window::EndDraw()
{
    this->m_window.display();
}