#include "../header/walker.h"

Walker::Walker(const float &l_radius, const sf::Vector2u &l_windowSize)
{
    this->m_xPos = l_windowSize.x / 2;
    this->m_yPos = l_windowSize.y / 2;
    this->m_windowSize = l_windowSize;
    this->m_radius = l_radius;

    this->Setup();

}

void Walker::Setup()
{
    this->m_walker.setRadius(this->m_radius);
    this->m_walker.setPosition(this->m_xPos, m_yPos);
    this->m_walker.setFillColor(sf::Color::Black);
    this->m_walker.setOrigin(this->m_radius, this->m_radius);
}

sf::Color Walker::GetNewColor()
{
    int red = rand() % 256;
    int blue = rand() % 256;
    int green = rand() % 256;

    return sf::Color(red, green, blue);
}

void Walker::SetNewPosition()
{
    float rand_num;

    do {
        rand_num = this->m_radius*(-1) + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX/(this->m_radius-this->m_radius*(-1))));
    } while (rand_num+this->m_xPos > this->m_windowSize.x || rand_num + this->m_xPos < 0);
    
    this->m_xPos += rand_num;

    do {
        rand_num = this->m_radius*(-1) + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX/(this->m_radius-this->m_radius*(-1))));
    } while (rand_num+this->m_yPos > this->m_windowSize.y || rand_num + this->m_yPos < 0);
    
    this->m_yPos += rand_num;
}

void Walker::Update() 
{
    this->SetNewPosition();
    this->m_walker.setPosition(this->m_xPos, this->m_yPos);
    this->m_walker.setFillColor(this->GetNewColor());
}