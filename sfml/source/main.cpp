#include "../header/window.h"
#include "../header/walker.h"

int main() 
{

    srand(time(NULL));

    Window window("Random Walker", sf::Vector2u(1280, 720), 60);
    Walker walker(5.f, window.GetWindowSize());
    
    window.BeginDraw();

    while (!window.IsDone())
    {
        // Update Section
        window.Update();
        walker.Update();

        // DrawSection
        window.Draw(walker.GetWalker());
        window.EndDraw();
    }

    return 0;
}