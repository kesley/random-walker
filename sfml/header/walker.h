#include <SFML/Graphics.hpp>

#ifndef WALKER_H
#define WALKER_H


class Walker
{

private:

    float m_xPos;
    float m_yPos;
    float m_radius;

    sf::Vector2u m_windowSize;
    sf::CircleShape m_walker;

    void Setup();
    void SetNewPosition();

    sf::Color GetNewColor();

public:
    Walker(const float &l_radius, const sf::Vector2u &l_windowSize);

    sf::CircleShape& GetWalker() { return this->m_walker; }

    void Update();

};

#endif