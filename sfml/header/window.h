#include <SFML/Graphics.hpp>
#include <string>

#ifndef WINDOW_H
#define WINDOW_H


class Window
{

private:

    bool m_isDone;

    int m_framerate;     
    std::string m_title;

    sf::Vector2u m_windowSize;
    sf::RenderWindow m_window;

    void Setup();

public:
    Window(const std::string& l_title, const sf::Vector2u l_size, const int& l_framerate);

    // Getter
    bool IsDone() const { return this->m_isDone; }
    sf::Vector2u GetWindowSize() const { return this->m_window.getSize(); }


    // Methods
    void Update();
    void BeginDraw();
    void Draw(sf::Drawable &l_drawnable);    
    void EndDraw();

};


#endif